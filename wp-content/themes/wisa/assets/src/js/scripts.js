(function($){
    $(document).ready(function() {
        
        const el = {
            header: $('header'),
            window: $(window),
            menu: {
                logo_hide: $('.menu_hide_logo_fixed'),
                logo_show: $('.menu_hide_logo_fixed')
            }
        }

        el.window.scroll(() => {
            if ($(this).scrollTop() > 20) {
                el.header.addClass('header_fixed');
            } else {
                el.header.removeClass('header_fixed');
            }
        }).scroll();


    });
})(jQuery);