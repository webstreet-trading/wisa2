<?php

// run includes
include('ws/vendor/cmb2/cmb2/init.php');
include('ws/divi/includes/cmb2_functions.php');
include('ws/divi/functions.php');

/**
 * Theme setup
 */
function ws_theme_setup() {
    
    add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);
    add_action('admin_enqueue_scripts', 'ws_admin_enqueue_css_js', 99);
    
    add_filter('ws_et_epanel_tabs', 'ws_et_epanel_tabs');
    add_filter('ws_et_epanel_subnav_ws', 'ws_et_epanel_subnav_ws');
}
add_action('init', 'ws_theme_setup');

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/dist/css/styles.css', [], ws('version'));

    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/dist/js/scripts.js', ['jquery'], ws('version'), true);
}

/**
 * Set admin styles and javascript
 */
function ws_admin_enqueue_css_js() {
    wp_enqueue_style('theme-admin-style', get_stylesheet_directory_uri() . '/assets/dist/css/admin_styles.css', [], ws('version'));
}

/**
 * Filter ws epanel tabs
 * 
 * @param array $tabs Defined tags
 * @return array
 */
function ws_et_epanel_tabs($tabs) {
    
    // add ws tab
    $tabs['ws'] = __('WS', 'ws');
    
    return $tabs;
}

/**
 * Get Subnavs for WS tab
 * 
 * @param array $subnavs Defined subnavs label => option
 * @return array
 */
function ws_et_epanel_subnav_ws($subnavs) {
    
    $subnavs[esc_html__('Layouts', 'ws')] = [
        [
            'name' => esc_html__('404 Page layout', 'ws'),
            'id' => '404_layout_divi_id',
            'type' => 'text',
            'desc' => esc_html__('Input the divi layout id that will output on the 404 page of the site.', 'ws'),
            'validation_type' => 'number'
        ],
    ];
    
    return $subnavs;
}