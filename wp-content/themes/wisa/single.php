<?php
get_header();

$prev_link = get_previous_post_link('%link', 'Older Posts');
$next_link = get_next_post_link('%link', 'Newest Posts');
?>

<div id="main-content">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		/**
		 * Fires before the title and post meta on single posts.
		 *
		 * @since 3.18.8
		 */
		do_action( 'et_before_post' );
		?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
			<div class="et_pb_section et_section_regular">
				<div class="et_pb_row">
					<div class="et_pb_column et_pb_css_mix_blend_mode_passthrough et_pb_column_4_4">
						<div class="et_pb_module et_pb_post_title et_pb_bg_layout_light et_pb_text_align_center">
							<div class="et_pb_title_container">
								<div class="et_pb_post_title_item">
									<h1 class="entry-title"><?php the_title(); ?></h1>
								</div>
								<div class="et_pb_post_title_item">
									<p class="et_pb_title_meta_container">
										<span class="meta_item published"><i class="icon icon_calendar"></i><span><?php the_date(); ?></span></span>
										<span class="meta_item author vcard"><i class="icon icon_profile"></i><span>by <?php the_author(); ?></span></span>
									</p>
								</div>
								<?php if (ws_isset(get_the_post_thumbnail('post-thumbnail'))) : ?>
								<div class="et_pb_post_title_item">
									<div class="et_pb_title_featured_container">
										<?php the_post_thumbnail('post-thumbnail'); ?>
									</div>
								</div>
								<?php endif; ?>
							</div>							
						</div>
					</div>
				</div>
			</div>

			<div class="et_pb_section et_section_regular">
				<div class="et_pb_row">
					<div class="et_pb_column et_pb_css_mix_blend_mode_passthrough et_pb_column_4_4">
						<div class="et_pb_module et_pb_text et_pb_bg_layout_light et_pb_text_align_center">
							<div class="entry-content et_pb_text_inner">
								<?php
									do_action( 'et_before_content' );
									the_content();
								?>
							</div> <!-- .entry-content -->
						</div>
					</div>
				</div>
			</div>

			<div class="et_pb_section et_section_regular">
				<div class="et_pb_row">
					<div class="et_pb_column et_pb_css_mix_blend_mode_passthrough et_pb_column_4_4">
						<div class="et_pb_module clearfix et_pb_posts_nav <?php echo ws_isset($prev_link) && ws_isset($next_link) ? 'nav_both' : ''; ?>">
							<?php if (ws_isset($prev_link)) : ?>
							<span class="nav_prev nav_container"><?php echo $prev_link; ?></span>
							<?php endif; ?>
							<?php if (ws_isset($next_link)) : ?>
							<span class="nav_next nav_container"><?php echo $next_link; ?></span>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</article> <!-- .et_pb_post -->
	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php
get_footer();
