<?php

class WS_Builder_Module_Blog extends ET_Builder_Module_Blog {

    /**
     * The string used to uniquely identify
     *
     * @var string
     */
    public $textdomain = null;

    /**
     * Init module
     */
    function init() {
        parent::init();

        $this->textdomain = WS::$textdomain;

        $this->name = esc_html__('Blog Post Type', $this->textdomain);
        $this->slug = 'ws_et_pb_blog';

        // $this->whitelisted_fields[] = 'post_type';
        $this->whitelisted_fields[] = 'taxonomy';
        $this->whitelisted_fields[] = 'include_taxonomies';
        // $this->whitelisted_fields[] = 'template';

        // $this->fields_defaults['post_type'] = array('post', 'add_default_setting');
        $this->fields_defaults['template'] = array('default.php');
    }

    /**
     * Get module fields
     */
    function get_fields() {
        $fields = parent::get_fields();

        // get post types
        $post_type_objects = get_post_types(array('public' => true), 'objects');
        $post_types = array();
        foreach ($post_type_objects as $post_type => $post_type_object) {
            $post_types[$post_type] = $post_type_object->labels->name;
        }

        // get templates
        $available_templates = WS::directory_files(dirname(WS::get_viewpath('modules/blog/default.php')));
        $templates = array_combine($available_templates, $available_templates);

        // add post type and taxononmy fields
        $fields = array_merge(array(
            // 'post_type' => array(
            //     'label' => esc_html__('Post Type', $this->textdomain),
            //     'type' => 'select',
            //     'option_category' => 'layout',
            //     'options' => $post_types,
            //     'toggle_slug' => 'main_content',
            //     'computed_affects' => array(
            //         '__posts',
            //     ),
            // ),
            'taxonomy' => array(
                'label' => esc_html__('Taxonomy slug', $this->textdomain),
                'description' => esc_html__('Taxononmy to use for below include.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'include_taxonomies' => array(
                'label' => esc_html__('Include Taxonomy slug(s)', $this->textdomain),
                'description' => esc_html__('Comma delimited list of taxonomy slug(s) to select. Leave empty to include all.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'template' => array(
                'label' => esc_html__('Template for posts', $this->textdomain),
                'description' => esc_html__('Template file to use to display a post', $this->textdomain),
                'type' => 'select',
                'option_category' => 'layout',
                'options' => $templates,
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
            'orderby' => array(
                'label' => esc_html__('Sorting', $this->textdomain),
                'description' => esc_html__('Sort order by settings', $this->textdomain),
                'type' => 'select',
                'option_category' => 'configuration',
                'options' => [
                    'date'   => esc_html__( 'Date', $this->textdomain ),
                    'name'   => esc_html__( 'Name', $this->textdomain ),
                    'slug'   => esc_html__( 'Slug', $this->textdomain ),
                    'id'   => esc_html__( 'ID', $this->textdomain ),
                ],
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
            'order' => array(
                'label' => esc_html__('Sorting', $this->textdomain),
                'description' => esc_html__('Sort posts order settings', $this->textdomain),
                'type' => 'select',
                'option_category' => 'configuration',
                'options' => [
                    'desc'   => esc_html__( 'DESC', $this->textdomain ),
                    'asc'   => esc_html__( 'ASC', $this->textdomain ),
                ],
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
        ), $fields);

        // $fields['__posts']['computed_depends_on'][] = 'post_type';
        $fields['__posts']['computed_depends_on'][] = 'taxonomy';
        $fields['__posts']['computed_depends_on'][] = 'include_taxonomies';
        $fields['__posts']['computed_depends_on'][] = 'template';

        // hide categories
        // $fields['include_categories']['toggle_slug'] = '';

        return $fields;
    }

    /**
	 * Get blog posts for blog module
	 *
	 * @param array   arguments that is being used by et_pb_blog
	 * @return string blog post markup
	 */
	static function get_blog_posts( $args = array(), $conditional_tags = array(), $current_page = array() ) {
		global $paged, $post, $wp_query, $et_fb_processing_shortcode_object, $et_pb_rendering_column_content;

		if ( self::$rendering ) {
			// We are trying to render a Blog module while a Blog module is already being rendered
			// which means we have most probably hit an infinite recursion. While not necessarily
			// the case, rendering a post which renders a Blog module which renders a post
			// which renders a Blog module is not a sensible use-case.
			return '';
		}

		$global_processing_original_value = $et_fb_processing_shortcode_object;

		// Default params are combination of attributes that is used by et_pb_blog and
		// conditional tags that need to be simulated (due to AJAX nature) by passing args
		$defaults = array(
			'use_current_loop'              => 'off',
			'post_type'                     => '',
			'fullwidth'                     => '',
			'posts_number'                  => '',
			'include_categories'            => '',
			'meta_date'                     => '',
			'show_thumbnail'                => '',
			'show_content'                  => '',
			'show_author'                   => '',
			'show_date'                     => '',
			'show_categories'               => '',
			'show_comments'                 => '',
			'show_excerpt'                  => '',
			'use_manual_excerpt'            => '',
			'excerpt_length'                => '',
			'show_pagination'               => '',
			'background_layout'             => '',
			'show_more'                     => '',
			'offset_number'                 => '',
			'masonry_tile_background_color' => '',
			'overlay_icon_color'            => '',
			'hover_overlay_color'           => '',
			'hover_icon'                    => '',
			'hover_icon_tablet'             => '',
			'hover_icon_phone'              => '',
			'use_overlay'                   => '',
			'header_level'                  => 'h2',
		);

		// WordPress' native conditional tag is only available during page load. It'll fail during component update because
		// et_pb_process_computed_property() is loaded in admin-ajax.php. Thus, use WordPress' conditional tags on page load and
		// rely to passed $conditional_tags for AJAX call
		$is_front_page               = et_fb_conditional_tag( 'is_front_page', $conditional_tags );
		$is_single                   = et_fb_conditional_tag( 'is_single', $conditional_tags );
		$et_is_builder_plugin_active = et_fb_conditional_tag( 'et_is_builder_plugin_active', $conditional_tags );
		$post_id                     = isset( $current_page['id'] ) ? (int) $current_page['id'] : 0;

		$container_is_closed = false;

		// remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
		remove_all_filters( 'wp_audio_shortcode_library' );
		remove_all_filters( 'wp_audio_shortcode' );
		remove_all_filters( 'wp_audio_shortcode_class' );

		$args = wp_parse_args( $args, $defaults );

		if ( 'on' === $args['use_current_loop'] ) {
			// Reset loop-affecting values to their defaults to simulate the current loop.
			$reset_keys = array( 'post_type', 'include_categories' );

			foreach ( $reset_keys as $key ) {
				$args[ $key ] = $defaults[ $key ];
			}
		}

		$processed_header_level = et_pb_process_header_level( $args['header_level'], 'h2' );
		$processed_header_level = esc_html( $processed_header_level );

		$overlay_output = '';

		if ( 'on' === $args['use_overlay'] ) {
			$overlay_output = ET_Builder_Module_Helper_Overlay::render( array(
				'icon'        => $args['hover_icon'],
				'icon_tablet' => $args['hover_icon_tablet'],
				'icon_phone'  => $args['hover_icon_phone'],
			) );
		}

		$overlay_class = 'on' === $args['use_overlay'] ? ' et_pb_has_overlay' : '';

		$query_args = array(
			'posts_per_page' => intval( $args['posts_number'] ),
			'post_status'    => 'publish',
			'post_type'      => $args['post_type'],
		);

		if ( defined( 'DOING_AJAX' ) && isset( $current_page['paged'] ) ) {
			$paged = intval( $current_page['paged'] ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		} else {
			$paged = $is_front_page ? get_query_var( 'page' ) : get_query_var( 'paged' ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		}

		// support pagination in VB
		if ( isset( $args['__page'] ) ) {
			$paged = $args['__page']; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		}

		$query_args['cat'] = implode( ',', self::filter_include_categories( $args['include_categories'], $post_id ) );

		$query_args['paged'] = $paged;

		if ( '' !== $args['offset_number'] && ! empty( $args['offset_number'] ) ) {
			/**
			 * Offset + pagination don't play well. Manual offset calculation required
			 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
			 */
			if ( $paged > 1 ) {
				$query_args['offset'] = ( ( $paged - 1 ) * intval( $args['posts_number'] ) ) + intval( $args['offset_number'] );
			} else {
				$query_args['offset'] = intval( $args['offset_number'] );
			}
		}

		if ( $is_single ) {
			$main_query_post = ET_Post_Stack::get_main_post();

			if ( null !== $main_query_post ) {
				$query_args['post__not_in'][] = $main_query_post->ID;
			}
		}

		// Get query
		$query = new WP_Query( $query_args );

		// Keep page's $wp_query global
		$wp_query_page = $wp_query;

		// Turn page's $wp_query into this module's query
		$wp_query = $query; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited

		self::$rendering = true;

		// Manually set the max_num_pages to make the `next_posts_link` work
		if ( '' !== $args['offset_number'] && ! empty( $args['offset_number'] ) ) {
			$wp_query->found_posts   = max( 0, $wp_query->found_posts - intval( $args['offset_number'] ) );
			$wp_query->max_num_pages = ceil( $wp_query->found_posts / intval( $args['posts_number'] ) );
		}

		ob_start();

		if ( $query->have_posts() ) {
			if ( 'on' !== $args['fullwidth'] ) {
				echo '<div class="et_pb_salvattore_content" data-columns>';
			}

			while( $query->have_posts() ) {
				$query->the_post();
				ET_Post_Stack::replace( $post );
				global $et_fb_processing_shortcode_object;

				$global_processing_original_value = $et_fb_processing_shortcode_object;

				// reset the fb processing flag
				$et_fb_processing_shortcode_object = false;

                // Print output                
                echo ws_view('modules/blog/' . $template, compact(
                    'overlay_class',
                    'fullwidth',
                    'show_thumbnail',
                    'post',
                    'use_overlay',
                    'overlay_output',
                    'processed_header_level',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'meta_date',
                    'show_content',
                    'show_more'
                ));

				$et_fb_processing_shortcode_object = $global_processing_original_value;
				ET_Post_Stack::pop();
			} // endwhile
			ET_Post_Stack::reset();

			if ( 'on' !== $args['fullwidth'] ) {
				echo '</div>';
			}

			if ( 'on' === $args['show_pagination'] ) {
				// echo '</div> <!-- .et_pb_posts -->'; // @todo this causes closing tag issue

				$container_is_closed = true;

				if ( function_exists( 'wp_pagenavi' ) ) {
					wp_pagenavi( array(
						'query' => $query
					) );
				} else {
					if ( $et_is_builder_plugin_active ) {
						include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
					} else {
						get_template_part( 'includes/navigation', 'index' );
					}
				}
			}
		}

		// Reset $wp_query to its origin
		$wp_query = $wp_query_page; // phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited

		if ( ! $posts = ob_get_clean() ) {
			$posts = self::get_no_results_template();
		}

		self::$rendering = false;

		return $posts;
	}

    /**
     * Remove module inner and module repeat
     * @param  string $output      [description]
     * @param  string $render_slug [description]
     * @return [type]              [description]
     */
    protected function _render_module_wrapper( $output = '', $render_slug = '' ) {
		return $output;
	}

    function render( $attrs, $content = null, $render_slug ) {
		global $post, $paged, $wp_query, $wp_filter, $__et_blog_module_paged;

		if ( self::$rendering ) {
			// We are trying to render a Blog module while a Blog module is already being rendered
			// which means we have most probably hit an infinite recursion. While not necessarily
			// the case, rendering a post which renders a Blog module which renders a post
			// which renders a Blog module is not a sensible use-case.
			return '';
		}

		// Stored current global post as variable so global $post variable can be restored
		// to its original state when et_pb_blog shortcode ends to avoid incorrect global $post
		// being used on the page (i.e. blog + shop module in backend builder)
		$post_cache = $post;

		/**
		 * Cached $wp_filter so it can be restored at the end of the callback.
		 * This is needed because this callback uses the_content filter / calls a function
		 * which uses the_content filter. WordPress doesn't support nested filter
		 */
		$wp_filter_cache = $wp_filter;

		$multi_view                          = et_pb_multi_view_options( $this );
		$use_current_loop                    = isset( $this->props['use_current_loop'] ) ? $this->props['use_current_loop'] : 'off';
		$post_type                           = isset( $this->props['post_type'] ) ? $this->props['post_type'] : 'post';
		$fullwidth                           = $this->props['fullwidth'];
		$posts_number                        = $this->props['posts_number'];
		$include_categories                  = $this->props['include_categories'];
		$meta_date                           = $this->props['meta_date'];
		$show_thumbnail                      = $this->props['show_thumbnail'];
		$show_content                        = $this->props['show_content'];
		$show_author                         = $this->props['show_author'];
		$show_date                           = $this->props['show_date'];
		$show_categories                     = $this->props['show_categories'];
		$show_comments                       = $this->props['show_comments'];
		$show_excerpt                        = $this->props['show_excerpt'];
		$use_manual_excerpt                  = $this->props['use_manual_excerpt'];
		$excerpt_length                      = $this->props['excerpt_length'];
		$show_pagination                     = $this->props['show_pagination'];
		$show_more                           = $this->props['show_more'];
		$offset_number                       = $this->props['offset_number'];
		$use_overlay                         = $this->props['use_overlay'];
        $header_level                        = $this->props['header_level'];
		
		$taxonomy 							= $this->props['taxonomy'];
        $include_taxonomies 				= $this->props['include_taxonomies'];
        $template                           = $this->props['template'];
        $orderby                            = $this->props['orderby'];
        $order                              = $this->props['order'];

		$masonry_tile_background_color_value = et_pb_responsive_options()->get_property_values( $this->props, 'masonry_tile_background_color' );
		$masonry_tile_background_color_hover = $this->get_hover_value( 'masonry_tile_background_color' );
		$overlay_icon_color_values           = et_pb_responsive_options()->get_property_values( $this->props, 'overlay_icon_color' );
		$hover_overlay_color_values          = et_pb_responsive_options()->get_property_values( $this->props, 'hover_overlay_color' );

		$background_layout                   = $this->props['background_layout'];
		$background_layout_hover             = et_pb_hover_options()->get_value( 'background_layout', $this->props, 'light' );
		$background_layout_hover_enabled     = et_pb_hover_options()->is_enabled( 'background_layout', $this->props );
		$background_layout_values            = et_pb_responsive_options()->get_property_values( $this->props, 'background_layout' );
		$background_layout_tablet            = isset( $background_layout_values['tablet'] ) ? $background_layout_values['tablet'] : '';
		$background_layout_phone             = isset( $background_layout_values['phone'] ) ? $background_layout_values['phone'] : '';

		$hover_icon                          = $this->props['hover_icon'];
		$hover_icon_values                   = et_pb_responsive_options()->get_property_values( $this->props, 'hover_icon' );
		$hover_icon_tablet                   = isset( $hover_icon_values['tablet'] ) ? $hover_icon_values['tablet'] : '';
		$hover_icon_phone                    = isset( $hover_icon_values['phone'] ) ? $hover_icon_values['phone'] : '';

		$video_background          = $this->video_background();
		$parallax_image_background = $this->get_parallax_image_background();

		$container_is_closed = false;

        $processed_header_level = et_pb_process_header_level( $header_level, 'h2' );

		// some themes do not include these styles/scripts so we need to enqueue them in this module to support audio post format
		wp_enqueue_style( 'wp-mediaelement' );
		wp_enqueue_script( 'wp-mediaelement' );

		// include easyPieChart which is required for loading Blog module content via ajax correctly
		wp_enqueue_script( 'easypiechart' );

		// include ET Shortcode scripts
		wp_enqueue_script( 'et-shortcodes-js' );

		// remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
		remove_all_filters( 'wp_audio_shortcode_library' );
		remove_all_filters( 'wp_audio_shortcode' );
		remove_all_filters( 'wp_audio_shortcode_class' );

		// Masonry Tile Background color.
		et_pb_responsive_options()->generate_responsive_css( $masonry_tile_background_color_value, '%%order_class%% .et_pb_blog_grid .et_pb_post', 'background-color', $render_slug, '', 'color' );

		if ( et_builder_is_hover_enabled( 'masonry_tile_background_color', $this->props ) ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%%:hover .et_pb_blog_grid .et_pb_post',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_html( $masonry_tile_background_color_hover )
				),
			) );
		}

		// Overlay Icon Color.
		et_pb_responsive_options()->generate_responsive_css( $overlay_icon_color_values, '%%order_class%% .et_overlay:before', 'color', $render_slug, '', 'color' );

		// Hover Overlay Color.
		et_pb_responsive_options()->generate_responsive_css( $hover_overlay_color_values, '%%order_class%% .et_overlay', 'background-color', $render_slug, '', 'color' );

		$overlay_output = '';

		if ( 'on' === $use_overlay ) {
			$overlay_output = ET_Builder_Module_Helper_Overlay::render( array(
				'icon'        => $hover_icon,
				'icon_tablet' => $hover_icon_tablet,
				'icon_phone'  => $hover_icon_phone,
			) );
		}

		$overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

		if ( 'on' !== $fullwidth ){
			wp_enqueue_script( 'salvattore' );

			$background_layout        = 'light';
			$background_layout_tablet = ! empty( $background_layout_tablet ) ? 'light' : '';
			$background_layout_phone  = ! empty( $background_layout_phone ) ? 'light' : '';
		}

		$args = array(
			'posts_per_page' => (int) $posts_number,
			'post_type'      => $post_type,
		);

		$et_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

		if ( $__et_blog_module_paged > 1 ) {
			$et_paged      = $__et_blog_module_paged;
			$paged         = $__et_blog_module_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
			$args['paged'] = $__et_blog_module_paged;
		}

		if ( is_front_page() ) {
			$paged = $et_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		}

		$args['cat'] = implode( ',', self::filter_include_categories( $include_categories ) );

		$args['paged'] = $et_paged;

		if ( '' !== $offset_number && ! empty( $offset_number ) ) {
			/**
			 * Offset + pagination don't play well. Manual offset calculation required
			 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
			 */
			if ( $paged > 1 ) {
				$args['offset'] = ( ( $et_paged - 1 ) * intval( $posts_number ) ) + intval( $offset_number );
			} else {
				$args['offset'] = intval( $offset_number );
			}
		}

		$main_query_post = ET_Post_Stack::get_main_post();

		if ( $main_query_post && is_singular( $main_query_post->post_type ) && ! isset( $args['post__not_in'] ) ) {
			$args['post__not_in'] = array( $main_query_post->ID );
		}

		// Images: Add CSS Filters and Mix Blend Mode rules (if set)
		if ( array_key_exists( 'image', $this->advanced_fields ) && array_key_exists( 'css', $this->advanced_fields['image'] ) ) {
			$this->add_classname( $this->generate_css_filters(
				$render_slug,
				'child_',
				self::$data_utils->array_get( $this->advanced_fields['image']['css'], 'main', '%%order_class%%' )
			) );
		}

		self::$rendering = true;

		$post_meta_remove_keys = array(
			'show_author',
			'show_date',
			'show_categories',
			'show_comments',
		);

		$post_meta_removes = array(
			'desktop' => array(
				'none' => 'none',
			),
			'tablet'  => array(
				'none' => 'none',
			),
			'phone'   => array(
				'none' => 'none',
			),
			'hover'   => array(
				'none' => 'none',
			),
		);

		foreach ( $post_meta_removes as $mode => $post_meta_remove ) {
			foreach ( $post_meta_remove_keys as $post_meta_remove_key ) {
				if ( $multi_view->has_value( $post_meta_remove_key, 'on', $mode, true ) ) {
					continue;
				}

				$post_meta_remove[ $post_meta_remove_key ] = $post_meta_remove_key;
			}

			$post_meta_removes[ $mode ] = implode( ',', $post_meta_remove );
		}

		$multi_view->set_custom_prop( 'post_meta_removes', $post_meta_removes );
		$multi_view->set_custom_prop( 'post_content', $multi_view->get_values( 'show_content' ) );

		$show_thumbnail = $multi_view->has_value( 'show_thumbnail', 'on' );

		ob_start();

		// Stash properties that will not be the same after wp_reset_query().
		$wp_query_props = array(
			'current_post' => $wp_query->current_post,
			'in_the_loop'  => $wp_query->in_the_loop,
		);

		$show_no_results_template = true;

		if ( 'off' === $use_current_loop ) {
			query_posts( $args );
		} elseif ( ! have_posts() || is_singular() ) {
			// Force an empty result set in order to avoid loops over the current post.
			query_posts( array( 'post__in' => array( 0 ) ) );
			$show_no_results_template = false;
		} else {
			// Only allow certain args when `Posts For Current Page` is set.
			global $wp_query;
			$original = $wp_query->query_vars;
			$custom   = array_intersect_key( $args, array_flip( array( 'posts_per_page', 'offset' ) ) );
			query_posts( array_merge( $original, $custom ) );
		}

		// Manually set the max_num_pages to make the `next_posts_link` work
		if ( '' !== $offset_number && ! empty( $offset_number ) ) {
			global $wp_query;
			$wp_query->found_posts   = max( 0, $wp_query->found_posts - intval( $offset_number ) );
			$wp_query->max_num_pages = ceil( $wp_query->found_posts / intval( $posts_number ) );
		}

        if ('' !== $taxonomy && '' !== $include_taxonomies) {
            $args['tax_query'] = [
				[
					'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => explode(', ', $include_taxonomies),
				]
			];
		}

        // order
        if ('' !== $orderby) {
            $order = '' === $order ? 'desc' : $order;

            $args['orderby'] = $orderby;
            $args['order'] = $order;
        }

        $template_class = str_replace('.php', '', $template);
        $module_class_inner = ' blog_inner blog_' . $template_class . '_inner et_pb_ajax_pagination_container';
		$this->add_classname('blog_' . $template_class);
		
		$state = '';
		$icon = '';
		if ($is_team = 'team' === $post_type) {

			$module_class_inner = 'et_pb_module accordian_team et_pb_accordion';
			$state = 'open';
			$icon = 'minus';

		}

		$args = apply_filters('ws_blog_post_type_args', $args, $template);

		query_posts($args);

		if ( have_posts() ) {
			if ( 'off' === $fullwidth ) {
				$attribute = et_core_is_fb_enabled() ? 'data-et-vb-columns' : 'data-columns';
				echo '<div class="et_pb_salvattore_content" ' . et_core_intentionally_unescaped( $attribute, 'fixed_string' ) . '>';
			}

			$count = 0;
			while ( have_posts() ) {
				the_post();
				ET_Post_Stack::replace( $post );

				if ($is_team && $count > 0) {
					$state = 'close';
					$icon = 'plus';
				}

                echo ws_view('modules/blog/' . $template, compact(
                    'overlay_class',
                    'fullwidth',
                    'show_thumbnail',
                    'post',
                    'use_overlay',
                    'overlay_output',
                    'processed_header_level',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'meta_date',
                    'show_content',
					'show_more',
					'state',
					'icon',
					'count'
				));
				
				$count++;

				ET_Post_Stack::pop();
			} // endwhile
			ET_Post_Stack::reset();

			if ( 'off' === $fullwidth ) {
				echo '</div><!-- .et_pb_salvattore_content -->';
			}

			if ( $multi_view->has_value( 'show_pagination', 'on' ) ) {

				echo '<div class="pagi_container">';
				$multi_view->render_element( array(
					'tag'        => 'div',
					'content'    => $this->render_pagination( false ),
					'visibility' => array(
						'show_pagination' => 'on',
					),
				), true );
				echo '</div>';

			   echo '</div> <!-- .et_pb_posts -->';

			   $container_is_closed = true;
		   }
		} elseif ( $show_no_results_template ) {
			if ( et_is_builder_plugin_active() ) {
				include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
			} else {
				get_template_part( 'includes/no-results', 'index' );
			}
		}

		wp_reset_query();
		ET_Post_Stack::reset();

		// Restore stashed properties.
		foreach ( $wp_query_props as $prop => $value ) {
			$wp_query->{$prop} = $value;
		}

		$posts = ob_get_contents();

		ob_end_clean();
		self::$rendering = false;

		// Remove automatically added classnames
		$this->remove_classname( array(
			$render_slug,
		) );

		$data_background_layout       = '';
		$data_background_layout_hover = '';

		if ( $background_layout_hover_enabled ) {
			$data_background_layout = sprintf(
				' data-background-layout="%1$s"',
				esc_attr( $background_layout )
			);
			$data_background_layout_hover = sprintf(
				' data-background-layout-hover="%1$s"',
				esc_attr( $background_layout_hover )
			);
		}

		if ( 'on' !== $fullwidth ) {
			// Module classname
			$this->add_classname( array(
				'et_pb_blog_grid_wrapper',
			) );

			// Remove auto-added classname for module wrapper because on grid mode these classnames
			// are placed one level below module wrapper
			$this->remove_classname( array(
				'et_pb_section_video',
				'et_pb_preload',
				'et_pb_section_parallax',
			) );

			// Inner module wrapper classname
			$inner_wrap_classname = array(
				'et_pb_blog_grid',
				'clearfix',
				"et_pb_bg_layout_{$background_layout}",
				$this->get_text_orientation_classname(),
			);

			if ( ! empty( $background_layout_tablet ) ) {
				array_push( $inner_wrap_classname, "et_pb_bg_layout_{$background_layout_tablet}_tablet" );
			}

			if ( ! empty( $background_layout_phone ) ) {
				array_push( $inner_wrap_classname, "et_pb_bg_layout_{$background_layout_phone}_phone" );
			}

			if ( '' !== $video_background ) {
				$inner_wrap_classname[] = 'et_pb_section_video';
				$inner_wrap_classname[] = 'et_pb_preload';
			}

			if ( '' !== $parallax_image_background ) {
				$inner_wrap_classname[] = 'et_pb_section_parallax';
			}

			$multi_view_data_attr = $multi_view->render_attrs( array(
				'classes' => array(
					'et_pb_blog_show_content' => array(
						'show_content' => 'on',
					),
				),
			) ) ;

			$output = sprintf(
				'<div%4$s class="%5$s"%9$s%10$s>
					<div class="%1$s">
					%7$s
					%6$s
					<div class="%12$s"%11$s>
						%2$s
					</div>
					%3$s %8$s
				</div>',
				esc_attr( implode( ' ', $inner_wrap_classname ) ),
				$posts,
				( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
				$this->module_id(),
				$this->module_classname( $render_slug ), // #5
				$video_background,
				$parallax_image_background,
				$this->drop_shadow_back_compatibility( $render_slug ),
				et_core_esc_previously( $data_background_layout ),
				et_core_esc_previously( $data_background_layout_hover ), // #10
                et_core_esc_previously( $multi_view_data_attr ),
                $module_class_inner
			);
		} else {
			// Module classname
			$this->add_classname( array(
				'et_pb_posts',
				"et_pb_bg_layout_{$background_layout}",
				$this->get_text_orientation_classname(),
			) );

			if ( ! empty( $background_layout_tablet ) ) {
				$this->add_classname( "et_pb_bg_layout_{$background_layout_tablet}_tablet" );
			}

			if ( ! empty( $background_layout_phone ) ) {
				$this->add_classname( "et_pb_bg_layout_{$background_layout_phone}_phone" );
			}

			$multi_view_data_attr = $multi_view->render_attrs( array(
				'classes' => array(
					'et_pb_blog_show_content' => array(
						'show_content' => 'on',
					),
				),
			) ) ;

			$output = sprintf(
				'<div%4$s class="%1$s"%8$s%9$s>
				%6$s
				%5$s
				<div class="%11$s"%10$s>
					%2$s
				</div>
				%3$s %7$s',
				$this->module_classname( $render_slug ),
				$posts,
				( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
				$this->module_id(),
				$video_background, // #5
				$parallax_image_background,
				$this->drop_shadow_back_compatibility( $render_slug ),
				et_core_esc_previously( $data_background_layout ),
				et_core_esc_previously( $data_background_layout_hover ),
                et_core_esc_previously( $multi_view_data_attr ), #10
                $module_class_inner
			);
		}

		// Restore $wp_filter
		$wp_filter = $wp_filter_cache; // phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		unset($wp_filter_cache);

		// Restore global $post into its original state when et_pb_blog shortcode ends to avoid
		// the rest of the page uses incorrect global $post variable
		$post = $post_cache; // phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited

		return $output;
	}

    /**
     * Since the styling file is not updated until the author updates the page/post,
     * we should keep the drop shadow visible.
     *
     * @param string $functions_name
     *
     * @return string
     */
    private function drop_shadow_back_compatibility($functions_name) {
        $utils = ET_Core_Data_Utils::instance();
        $atts = $this->shortcode_atts;

        if (
            version_compare($utils->array_get($atts, '_builder_version', '3.0.93'), '3.0.94', 'lt') &&
            'on' !== $utils->array_get($atts, 'fullwidth') &&
            'on' === $utils->array_get($atts, 'use_dropshadow')
        ) {
            $class = self::get_module_order_class($functions_name);

            return sprintf(
                '<style>%1$s</style>', sprintf('.%1$s  article.et_pb_post { box-shadow: 0 1px 5px rgba(0,0,0,.1) }', esc_html($class))
            );
        }

        return '';
    }

}

new WS_Builder_Module_Blog();
