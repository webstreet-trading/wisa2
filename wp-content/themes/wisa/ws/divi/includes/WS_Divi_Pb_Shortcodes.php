<?php

/**
 * Contains divi shortcodes
 */
class WS_Divi_Pb_Shortcodes
{

    /**
     * Returns divi section shortcode
     * @param  string   $content    Content to appear within the divi section
     * @param  array    $args       Settings to add to the section id etc.
     * @return string   $shortcode  Shortcode
     */
    public function section($content, $args = []) {

        $class = isset($args['module_class']) ? 'module_class="' . $args['module_class'] . '"' : '';

        $shortcode  = '';
        $shortcode .= '[et_pb_section fb_built="1" ' . $class . ' _builder_version="3.21.3"]';
        $shortcode .= $content;
        $shortcode .= '[/et_pb_section]';

        return $shortcode;

    }

    /**
     * Returns a divi row
     * @param  string   $content    Content to appear within the divi row
     * @param  array    $args       Settings to add to the row id etc.
     * @return string   $shortcode  Shortcode
     */
    public function row($content, $args = []) {

        $class = isset($args['module_class']) ? 'module_class="' . $args['module_class'] . '"' : '';
        $class = isset($args['make_fullwidth']) ? 'make_fullwidth="' . $args['make_fullwidth'] . '"' : '';

        $shortcode  = '[et_pb_row ' . $class . ' _builder_version="3.21.3"]';
        $shortcode .= $content;
        $shortcode .= '[/et_pb_row]';

        return $shortcode;

    }

    /**
     * Returns a divi column
     * @param  string   $content    Content to appear within the divi column
     * @param  array    $args       Settings to add to the column id etc.
     * @return string   $shortcode  Shortcode
     */
    public function column($content, $args = ['type' => '4_4']) {

        $type = ws_isset($args['type']) ? $args['type'] : '4_4';

        $shortcode  = '[et_pb_column type="' . $type . '" _builder_version="3.21.3"]';
        $shortcode .= $content;
        $shortcode .= '[/et_pb_column]';

        return $shortcode;

    }

    /**
     * Returns a divi button module
     * @param  string   $text       The button text
     * @param  string   $href       The button url to display
     * @param  array    $args       Settings to add to the module id etc.
     * @return string   $shortcode  Shortcode
     */
    public function module_button($text, $href, $args = []) {

        $align  = isset($args['align']) ? 'button_alignment="' . $args['align'] . '"' : 'button_alignment="left"';
        $class  = isset($args['module_class']) ? 'module_class="' . $args['module_class'] . '"' : '';
        $target = isset($args['url_new_window']) ? 'url_new_window="' . $args['url_new_window'] . '"' : '';

        return '[et_pb_button _builder_version="3.21.3"
                    button_text="' . $text . '"
                    button_url="' . $href . '"
                    ' . $align . '
                    z_index_tablet="500"
                    ' . $class . '
                    url_new_window="on" /]';

    }

    /**
     * Returns a divi image module
     * @param  string   $src        The image url to display
     * @param  array    $args       Settings to add to the module id etc.
     * @return string   $shortcode  Shortcode
     */
    public function module_image($src, $args = []) {

        $align  = isset($args['align']) ? 'align="' . $args['align'] . '"' : '';

        return '[et_pb_image ' . $align . ' src="' . $src . '" _builder_version="3.21.3" /]';

    }

    /**
     * Returns a divi post nav module
     * @param  array    $args       Settings to add to the module id etc.
     * @return string   $shortcode  Shortcode
     */
    public function module_post_nav($args = ['prev_text' => 'Previous', 'next_text' => 'Next']) {

        $prev_text = isset($args['prev_text']) ? $args['prev_text'] : '';
        $next_text = isset($args['next_text']) ? $args['next_text'] : '';

        $shortcode  = '[et_pb_post_nav _builder_version="3.21.3" prev_text="' . $prev_text . '" next_text="' . $next_text . '" /]';

        return $shortcode;

    }

    /**
     * Returns a divi text module
     * @param  string   $content    Content to appear within the divi text module
     * @param  array    $args       Settings to add to the module id etc.
     * @return string   $shortcode  Shortcode
     */
    public function module_text($content, $args = []) {

        $text_align = isset($args['text_orientation']) ? 'text_orientation="' . $args['text_orientation'] . '"' : '';

        $shortcode  = '[et_pb_text ' . $text_align . ' _builder_version="3.21.3" text_font="||||||||"]';
        $shortcode .= $content;
        $shortcode .= '[/et_pb_text]';

        return $shortcode;

    }

}
