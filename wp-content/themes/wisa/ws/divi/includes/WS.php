<?php

if (!class_exists('WS')) {

/**
 * Webstreet class
 */
class WS {
    
    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
	public static $textdomain = 'ws';
    
    /**
     * The current version
     * 
     * @var string 
     */
	public static $version = '1.0.0';
    
    /**
     * Setup textdomain and version
     * 
     * @param string $textdomain
     * @param string $version
     */
    public static function setup($textdomain, $version) {
        self::$textdomain = $textdomain;
        self::$version = $version;
    }
    
    /**
     * Get path for view
     * 
     * @param string $view View to get path for
     * @return string View's full path
     */
    public static function get_viewpath($view) {
        
        // whether or not .php was added
        $view_file = preg_replace('/.php$/', '', $view) . '.php';	

        $theme_file = locate_template('ws-view/' . $view_file);
        if ($theme_file) {
            $file = $theme_file;
        }
        else {
            $file = get_stylesheet_directory() . '/ws/views/' . $view_file;
        }		

        return apply_filters('ws_template_' . $view_file, $file);
    }
    
    /**
     * Get view
     * 
     * @param string $view The view
     * @param array $vars View arguments
     * @return string View content
     */
    public static function view($view, $vars = array()) {
        
        // set domain
        $domain = WS::$textdomain;

        // get view path
        $viewpath = self::get_viewpath($view);
        if ($viewpath && is_file($viewpath)) {
            extract($vars);
            ob_start();
            include($viewpath);
            $content = ob_get_contents();
            ob_end_clean();
        }
        // view not found
        else {
            $content = sprintf(__('Unable to locate view for: %s', $domain), $view);
        }
        return $content;
    }
    
    /**
     * Directory files
     * 
     * Scan directory for files
     * 
     * @param string $directory Directory to scan
     * @param boolean $folders Return folders. Default false
     * @return array Files in directory
     */
    public static function directory_files($directory, $folders = false) {
        
        $results = scandir($directory);
        $files = array();
        foreach ($results as $file) {
            
            // filter out invalid files
            if ($file === '.' || $file === '..') {
                continue;
            }
            
            // exclude folders
            if (!$folders && is_dir($directory . '/' . $file)) {
                continue;
            }
            
            // add file
            $files[] = $file;
        }
        
        return $files;
    }
}

}
