<?php
/**
 * Default Blog output
 *
 * Override this template by copying it to yourtheme/ws-view/modules/blog/default.php
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$no_thumb_class = '' === get_the_post_thumbnail(get_the_ID(), 'post-thumbnail') ? 'et_pb_post_item_img_empty' : '';
$view_name = ' view_blog view_' . basename(__FILE__, '.php');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post clearfix' . $view_name); ?>>
    <div class="et_pb_post_container et_pb_post_container_img">
        <div class="et_pb_post_item et_pb_post_item_img <?= $no_thumb_class; ?>">
            <a href="<?php the_permalink(); ?>" class="img_wrap ">
                <?php the_post_thumbnail('post-thumbnail'); ?>
            </a>
        </div>
    </div>
    <div class="et_pb_post_container et_pb_post_container_content">
        <div class="et_pb_post_item et_pb_post_item_title">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </div>
        <div class="et_pb_post_item et_pb_post_item_content">
            <?php
            if ('on' === $show_content) {

                echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));

            } else {

                echo wpautop(get_the_excerpt());

            }
            ?>
        </div>
    </div>
    <div class="et_pb_post_container et_pb_post_container_btn">
        <div class="et_pb_button_wrapper">
            <a href="<?php the_permalink(); ?>" class="et_pb_button btn_full"><?= __('Read More', 'ws'); ?></a>
        </div>
    </div>
</article> <!-- .et_pb_post -->